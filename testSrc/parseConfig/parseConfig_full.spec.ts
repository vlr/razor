import { expect } from "chai";
import { parse } from "../../src/parse/parse";
import { ParsedConfig, parsedImport } from "../../src/parse/types/parsedConfig.type";

describe("parseConfig", function (): void {
  it("should parse both imports and definition", function (): void {
    // arrange
    const source = `@import { a } from 'b'
@define imports(a: number)
`;
    const expected: ParsedConfig = {
      imports: [parsedImport(0, "{ a } from 'b'")],
      definition: {
        position: 23,
        parameters: "a: number",
        name: "imports"
      }
    };

    // act
    const result = parse(source).config;

    // assert
    expect(result).deep.equals(expected);
  });

  it("should work with spaces in between and stop at content line", function (): void {
    // arrange
    // tslint:disable:no-trailing-whitespace
    const source = ` 
@import { a } from 'b'

@define imports(a: number)
 
@import { c } from 'd'
 
 unknown`;
    // tslint:enable:no-trailing-whitespace
    const expected: ParsedConfig = {
      imports: [
        parsedImport(2, "{ a } from 'b'"),
        parsedImport(55, "{ c } from 'd'")
      ],
      definition: {
        position: 26,
        parameters: "a: number",
        name: "imports"
      }
    };

    // act
    const result = parse(source).config;

    // assert
    expect(result).deep.equals(expected);
  });

});
