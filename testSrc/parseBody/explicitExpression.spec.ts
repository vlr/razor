import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { expect } from "chai";

describe("parser/explicitExpression", function (): void {

    it("should return expression node", function (): void {
        // arrange
        const src = "aaa@(bbb) ccc";

        // act
        const res = parse(src);

        // assert
        expect(res.content.length).equals(3);
        expectNode.literal(res.content[0], "aaa");
        expectNode.expression(res.content[1], "bbb");
        expectNode.literal(res.content[2], " ccc");
    });


    it("should return 2 expression nodes in a row", function (): void {
        // arrange
        const src = "@(bbb)@(ccc)";

        // act
        const res = parse(src);

        // assert
        expect(res.content.length).equals(2);
        expectNode.expression(res.content[0], "bbb");
        expectNode.expression(res.content[1], "ccc");
    });


    it("should return deep brackets in explicit expression", function (): void {
        // arrange
        const src = '@(bbb<as>(sadf[new a{\'b\' = "x"}]))';

        // act
        const res = parse(src);

        // assert
        expect(res.content.length).equals(1);
        expectNode.expression(res.content[0], 'bbb<as>(sadf[new a{\'b\' = "x"}])');
    });
});
