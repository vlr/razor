import { expect } from "chai";
import { BasicNode, NodeType, ContentNode, ForNode, IfNode, PartialNode } from "../../src/parse/nodes/nodes.type";

function expectNodeType(node: BasicNode, type: NodeType): void {
  expect(node.type).to.be.equal(type);
}

function contentNode(node: BasicNode, content: string, type: NodeType): void {
  expectNodeType(node, type);
  const contentNode = node as ContentNode;
  expect(contentNode.content).to.be.equal(content);
}

export function comment(node: BasicNode, content: string): void {
  contentNode(node, content, NodeType.Comment);
}

export function literal(node: BasicNode, content: string): void {
  contentNode(node, content, NodeType.Literal);
}

export function quote(node: BasicNode): void {
  expectNodeType(node, NodeType.Quote);
}

export function apostrophe(node: BasicNode): void {
  expectNodeType(node, NodeType.Apostrophe);
}

export function expression(node: BasicNode, content: string): void {
  contentNode(node, content, NodeType.Expression);
}

export function injection(node: BasicNode, content: string): void {
  contentNode(node, content, NodeType.Injection);
}

export function eol(node: BasicNode): void {
  expectNodeType(node, NodeType.Eol);
}

export function forceEol(node: BasicNode): void {
  expectNodeType(node, NodeType.ForceEol);
}


export function forEach(node: BasicNode, condition: string, childCount?: number): ForNode {
  expectNodeType(node, NodeType.For);
  const forEachNode = node as ForNode;
  expect(forEachNode.condition).to.be.equal(condition);
  if (childCount) {
    expect(forEachNode.children.length).to.be.equal(childCount, "child count in forEach node is not as expected");
  }
  return forEachNode;
}

export function ifNode(node: BasicNode, condition: string): IfNode {
  expectNodeType(node, NodeType.If);
  const ifNode = node as IfNode;
  expect(ifNode.condition).to.be.equal(condition);
  return ifNode;
}

export function ifChildren(node: IfNode, ifCount: number, elseCount: number = 0): void {
  expect(node.ifChildren.length).to.be.equal(ifCount, "if node children count is not as expected");
  expect(node.elseChildren.length).to.be.equal(elseCount, "if node children else count is not as expected");
}

export function partial(node: BasicNode, name: string, parameters: string, indent: string): void {
  expectNodeType(node, NodeType.Partial);
  const partialNode = node as PartialNode;
  expect(partialNode.name).to.be.equal(name);
  expect(partialNode.parameters).to.be.equal(parameters);
  expect(partialNode.indent).to.be.equal(indent);
}
