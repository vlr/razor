import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { expect } from "chai";

describe("parser/literal", function (): void {
  it("should return literal node", function (): void {
    // arrange
    const src = "using asdf fre arfs";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(1);
    expectNode.literal(res.content[0], src);
  });


  it("should return 2 literal nodes and eol in between", function (): void {
    // arrange
    const src = "aaa\nbbb";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(3);
    expectNode.literal(res.content[0], "aaa");
    expectNode.eol(res.content[1]);
    expectNode.literal(res.content[2], "bbb");
  });


  it("should treat windows linebreaks the same way", function (): void {
    // arrange
    const src = "aaa\r\nbbb";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(3);
    expectNode.literal(res.content[0], "aaa");
    expectNode.eol(res.content[1]);
    expectNode.literal(res.content[2], "bbb");
  });

  it("should treat mac linebreaks the same way", function (): void {
    // arrange
    const src = "aaa\rbbb";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(3);
    expectNode.literal(res.content[0], "aaa");
    expectNode.eol(res.content[1]);
    expectNode.literal(res.content[2], "bbb");
  });


  it("should return 7 nodes", function (): void {
    // arrange
    const src = `aaa
        bbb
        ccc
        ddd`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(7);
  });


  it("should skip empty lines before literal", function (): void {
    // arrange
    const src = `

   aaa`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(1);
    expectNode.literal(res.content[0], "   aaa");
  });


//   it("should not skip empty lines with force eol", function (): void {
//     // arrange
//     const src = `
//    @eol
//    aaa`;

//     // act
//     const res = parse(src);

//     // assert
//     expect(res.content.length).equals(3);

//     expectNode.literal(res.content[0], "   ");
//     expectNode.forceEol(res.content[1]);
//     expectNode.literal(res.content[2], "   aaa");
//   });


//   it("should skip empty literal and eol after force eol", function (): void {
//     // arrange
//     const src = `
//    @eol
// `;

//     // act
//     const res = parse(src);

//     // assert
//     expect(res.content.length).equals(2);
//     expectNode.literal(res.content[0], "   ");
//     expectNode.forceEol(res.content[1]);
//   });

//   it("should not skip non-empty literal after force eol", function (): void {
//     // arrange
//     const src = `
// @eol   aaa
// `;

//     // act
//     const res = parse(src);

//     // assert
//     expect(res.content.length).equals(3);
//     expectNode.forceEol(res.content[0]);
//     expectNode.literal(res.content[1], "   aaa");
//     expectNode.eol(res.content[2]);
//   });

  it("should return quotes as quote node", function (): void {
    // arrange
    const src = `abc("def");`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(5);
    expectNode.literal(res.content[0], "abc(");
    expectNode.quote(res.content[1]);
    expectNode.literal(res.content[2], "def");
    expectNode.quote(res.content[3]);
    expectNode.literal(res.content[4], ");");
  });

  it("should return apostrophes as quote node", function (): void {
    // arrange
    const src = `abc('def');`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(5);
    expectNode.literal(res.content[0], "abc(");
    expectNode.apostrophe(res.content[1]);
    expectNode.literal(res.content[2], "def");
    expectNode.apostrophe(res.content[3]);
    expectNode.literal(res.content[4], ");");
  });

  it("should return escaped apostrophe and quote as literal", function (): void {
    // arrange
    const src = `abc(@'def@");`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(1);

    expectNode.literal(res.content[0], `abc('def");`);
  });
});
