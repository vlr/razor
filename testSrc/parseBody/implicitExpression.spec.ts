import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { expect } from "chai";

describe("parser/implicitExpression", function (): void {

  it("should treat @@ as escaped @", function (): void {
    // arrange
    const src = "aaa@@bbb ccc";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(1);
    expectNode.literal(res.content[0], "aaa@bbb ccc");
  });


  it("should return expression node", function (): void {
    // arrange
    const src = "aaa@bbb ccc";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(3);
    expectNode.literal(res.content[0], "aaa");
    expectNode.expression(res.content[1], "bbb");
    expectNode.literal(res.content[2], " ccc");
  });


  it("should return 2 expression nodes in a row", function (): void {
    // arrange
    const src = "@bbb@ccc";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(2);
    expectNode.expression(res.content[0], "bbb");
    expectNode.expression(res.content[1], "ccc");
  });


  it("should return expression node and eol node", function (): void {
    // arrange
    const src = "@bbb\nccc";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(3);
    expectNode.expression(res.content[0], "bbb");
    expectNode.eol(res.content[1]);
    expectNode.literal(res.content[2], "ccc");
  });

  it("should return implicit expression with content in brackets", function (): void {
    // arrange
    const src = "aaa @bbb[vvv].ccc asd";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(3);
    expectNode.literal(res.content[0], "aaa ");
    expectNode.expression(res.content[1], "bbb[vvv].ccc");
    expectNode.literal(res.content[2], " asd");
  });


  it("should return (at) as part of literal if expressions has no meaningful characters", function (): void {
    // arrange
    const src = "@)";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(1);
    expectNode.literal(res.content[0], src);
  });
});
