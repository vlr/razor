import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { expect } from "chai";

describe("parser/partial", function (): void {

  it("should return partial node with no indent", function (): void {
    // arrange
    const src = "@[aaa(bbb)]";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(1);
    expectNode.partial(res.content[0], "aaa", "bbb", "");
  });

  it("should return partial node with indent", function (): void {
    // arrange
    const src = "   @[aaa(bbb)]";

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(1);
    expectNode.partial(res.content[0], "aaa", "bbb", "   ");
  });


  it("should return partial node with indent after eol", function (): void {
    // arrange
    const src = `hi
    @[aaa(bbb)]`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(3);
    expectNode.literal(res.content[0], "hi");
    expectNode.eol(res.content[1]);
    expectNode.partial(res.content[2], "aaa", "bbb", "    ");
  });

  it("should ignore eol if return partial node with indent after eol", function (): void {
    // arrange
    const src = `hi
    @[aaa(bbb)]
    abc`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(4);
    expectNode.literal(res.content[0], "hi");
    expectNode.eol(res.content[1]);
    expectNode.partial(res.content[2], "aaa", "bbb", "    ");
    expectNode.literal(res.content[3], "    abc");
  });

  it("should ignore eol if return partial node without indent", function (): void {
    // arrange
    const src = `
@[aaa(bbb)]
    abc`;

    // act
    const res = parse(src);

    // assert
    expect(res.content.length).equals(2);
    expectNode.partial(res.content[0], "aaa", "bbb", "");
    expectNode.literal(res.content[1], "    abc");
  });
});
