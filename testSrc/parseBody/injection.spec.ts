import * as expectNode from "./expectNode";
import { parse } from "../../src/parse/parse";
import { expect } from "chai";

describe("parser/injection", function (): void {

    it("should return injection node", function (): void {
        // arrange
        const src = "aaa@{let a = b} ccc";

        // act
        const res = parse(src);

        // assert
        expect(res.content.length).equals(3);
        expectNode.literal(res.content[0], "aaa");
        expectNode.injection(res.content[1], "let a = b");
        expectNode.literal(res.content[2], " ccc");
    });


    it("should return deep brackets in injection", function (): void {
        // arrange
        const src = `@{let x = bbb<as>(sadf[new a{\'b\' = "x"}]);}`;

        // act
        const res = parse(src);

        // assert
        expect(res.content.length).equals(1);
        expectNode.injection(res.content[0], 'let x = bbb<as>(sadf[new a{\'b\' = "x"}]);');
    });

    it("should drop all spaces if injection is only thing on the line", function (): void {
      // arrange
      // tslint:disable:no-trailing-whitespace
      const src = `abc
      @{let x = y;}     
def`;
      // act
      const res = parse(src);

      // assert
      expect(res.content.length).equals(4);
      expectNode.literal(res.content[0], "abc");
      expectNode.eol(res.content[1]);      
      expectNode.injection(res.content[2], "let x = y;");
      expectNode.literal(res.content[3], "def");
  });
});
