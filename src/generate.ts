import { parse } from "./parse/parse";
import { produce } from "./produce/produce";
import { RazorOptions } from "./razorOptions.type";

function defaultOptions(options: RazorOptions): RazorOptions {
  return {
    linefeed: "\n",
    quotes: "\"",
    language: "ts",
    ...options
  };
}

export function generate(template: string, options: RazorOptions = {}): string {
  options = defaultOptions(options);
  const root = parse(template);
  return produce(root, options);
}
