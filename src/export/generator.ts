import { LineFeed, Quotes, GeneratorOptions } from "./generatorOptions.type";

interface GenOptions {
  linefeed: LineFeed;
  quoteChar: Quotes;
  apostropheChar: Quotes;
}

export class Generator {
  public indent: string = "";
  private options: GenOptions;
  private lines: string[] = [];
  private eolPrinted: boolean = true;

  constructor(options: GeneratorOptions) {
    this.options = {
      linefeed: options.linefeed || "\n",
      quoteChar: options.quotes || "\"",
      apostropheChar: options.quotes || "'"
    };
  }

  public setIndent(indent: string): Generator {
    this.indent = indent;
    return this;
  }

  public append(text: string): Generator {
    if (this.eolPrinted) { this.lines.push(this.indent); }
    this.lines.push(text);
    this.eolPrinted = false;
    return this;
  }

  public eol(): Generator {
    this.eolPrinted = true;
    this.lines.push(this.options.linefeed);
    return this;
  }

  public quote(): Generator {
    return this.append(this.options.quoteChar);
  }

  public apostrophe(): Generator {
    return this.append(this.options.apostropheChar);
  }


  public toString(): string {
    return this.lines.join("");
  }
}
