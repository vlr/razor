import { GeneratorOptions } from "./export";

export type LineFeed = "\n" | "\r\n" | "\r";
export type Language = "ts" | "js";

export interface RazorOptions extends GeneratorOptions {
  language?: Language;
}
