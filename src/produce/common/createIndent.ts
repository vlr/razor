import { indentSize } from "./constants";

const indents = new Map<number, string>();

export function createIndent(amount: number = 1): string {
  if (indents[amount]) { return indents[amount]; }
  return indents[amount] = new Array(amount * indentSize).fill(" ").join("");
}
