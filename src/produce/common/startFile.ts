import { StringGen } from "./stringGen";
import { libMark } from "../../libMark";

export function startFile(lineFeed: string): StringGen {
  let gen = new StringGen(lineFeed);
  gen = gen.appendLine(libMark(lineFeed));
  return gen.appendLine();
}
