import { Parsed } from "../parse/types";
import { RazorOptions } from "../razorOptions.type";
import { produceTs } from "./ts/produceTs";

export function produce(parsed: Parsed, options: RazorOptions): string {
  switch (options.language) {
    case "ts":
      return produceTs(parsed, options);
    default:
      return "";
  }
}
