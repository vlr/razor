import { Parsed } from "../../parse/types";

export function paramsLine(parsed: Parsed): string {
  return prefixedParams(parsed.config.definition.parameters);
}

export function prefixedParams(params: string): string {
  return params.length > 0
    ? params + ", "
    : "";
}
