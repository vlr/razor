import { StringGen } from "../../common/stringGen";
import { BasicNode, NodeType, ContentNode, PartialNode, ForNode, IfNode } from "../../../parse/nodes/nodes.type";
import { literal, eol, quote, apostrophe, expression, injection, partial, forNode, ifNode } from "./nodes";

export function generateNode(gen: StringGen, node: BasicNode): StringGen {
  switch (node.type) {
    case NodeType.Literal:
      return literal(gen, node as ContentNode);
    case NodeType.Eol:
      return eol(gen);
    case NodeType.Quote:
      return quote(gen);
    case NodeType.Apostrophe:
      return apostrophe(gen);
    case NodeType.Expression:
      return expression(gen, node as ContentNode);
    case NodeType.Injection:
      return injection(gen, node as ContentNode);
    case NodeType.Partial:
      return partial(gen, node as PartialNode);
    case NodeType.For:
      return forNode(gen, node as ForNode);
    case NodeType.If:
      return ifNode(gen, node as IfNode);
    default:
      return gen;
  }
}
