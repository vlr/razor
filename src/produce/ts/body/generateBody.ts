import { Parsed } from "../../../parse/types";
import { StringGen } from "../../common/stringGen";
import { generateNode } from "./generateNode";

export function generateBody(gen: StringGen, parsed: Parsed): StringGen {
  gen = gen.appendLine(`const indent = gen.indent;`);
  gen = parsed.content.reduce(generateNode, gen);
  gen = gen.appendLine(`gen = gen.setIndent(indent);`);
  gen = gen.appendLine(`return gen;`);
  return gen;
}
