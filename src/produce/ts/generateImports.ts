import { StringGen } from "../common/stringGen";
import { ParsedImport, Parsed } from "../../parse/types";
import { RazorOptions } from "../../razorOptions.type";

export function generateImports(gen: StringGen, parsed: Parsed, options: RazorOptions): StringGen {
  gen = gen.appendLine(`import { Generator, GeneratorOptions } from ${options.quotes}@vlr/razor/export${options.quotes};`);
  return parsed.config.imports.reduce(generateImport, gen);
}

function generateImport(gen: StringGen, imp: ParsedImport): StringGen {
  return gen.appendLine(`import ${imp.content}`);
}
