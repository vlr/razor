import { Parsed } from "../../parse/types";
import { StringGen } from "../common/stringGen";
import { paramsLine } from "./paramsLine";

export function generateEntryFunction(gen: StringGen, parsed: Parsed): StringGen {
  gen = gen.append(`function generate(${paramsLine(parsed)}options: GeneratorOptions): string `);
  return gen.braces(gen => gen.appendLine(`return generateContent(${getParametersLine(parsed)}, new Generator(options)).toString();`));
}

function getParametersLine(parsed: Parsed): string {
  const parms = parsed.config.definition.parameters.split(", ");
  return parms.map(stripType).join(", ");
}

function stripType(parm: string): string {
  const semicolon = parm.indexOf(":");
  if (semicolon === -1) { return parm; }
  return parm.slice(0, semicolon).trim();
}
