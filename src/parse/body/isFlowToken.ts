import { toMap } from "@vlr/map-tools";
import { contentTokens, EscapeBrace, Else } from "../tokens";
import { Token } from "../types";
import { except } from "@vlr/array-tools";

const flowTokens = except(contentTokens, [EscapeBrace, Else]);

const rootMap = toMap(flowTokens, t => t);

const childTokens = [...flowTokens, "}"];

const childMap = toMap(childTokens, t => t);

export type isFlowToken = (token: Token) => boolean;

export function isRootFlowToken(token: Token): boolean {
  return rootMap.has(token.token);
}

export function isChildFlowToken(token: Token): boolean {
  return childMap.has(token.token);
}
