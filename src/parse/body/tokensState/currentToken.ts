import { Token } from "../../types";
import { TokensState } from "./tokensState.type";

function currentTokenObj(state: TokensState): Token {
  return state.tokens[state.currentIndex];
}

export function currentToken(state: TokensState): string {
  return currentTokenObj(state).token;
}

export function currentPosition(state: TokensState): number {
  return currentTokenObj(state).position;
}
