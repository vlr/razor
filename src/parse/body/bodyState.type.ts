import { BasicNode } from "../nodes/nodes.type";
import { TokensState } from "./tokensState/tokensState.type";

export interface BodyState extends TokensState {
  nodes: BasicNode[];
  endReached: boolean;
  isMultiLine: boolean;
  eatsEol: boolean;
}
