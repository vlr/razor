import { ForNode } from "../../../nodes/nodes.type";
import { BodyState } from "../../bodyState.type";
import { currentPosition } from "../../tokensState/currentToken";
import { addNode } from "../addNode";
import { treatTokenAsLiteral } from "../content/addLiteral";
import { removeIndent } from "../getIndent";
import { getBracedChildNodes } from "./getBracedChildNodes";
import { getCondition } from "./getCondition";

export function addForNode(state: BodyState): BodyState {
  const condition = getCondition(state.tokens, state.currentIndex);
  const children = getBracedChildNodes(state.tokens, condition.nextIndex);
  if (!condition.success || !children.success) {
    return treatTokenAsLiteral(state);
  }

  if (children.isMultiline) { state = removeIndent(state); }
  const node = new ForNode(condition.condition, children.nodes, currentPosition(state));
  return addNode(state, children.nextIndex, node);
}
