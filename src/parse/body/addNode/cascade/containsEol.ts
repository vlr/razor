import { BasicNode } from "../../../nodes/nodes.type";
import { isEolNode } from "../isEolNode";

export function containsEol(nodes: BasicNode[]): boolean {
  return nodes.some(isEolNode);
}
