import { Apostrophe, At, AtBrace, AtBracket, AtParenthesis, LineFeed1, LineFeed2, LineFeed3, Quote, For, Foreach, If } from "../../tokens";
import { BodyState } from "../bodyState.type";
import { currentToken } from "../tokensState/currentToken";
import { addApostropheNode, addEolNode, addQuoteNode } from "./basic/addBasicNode";
import { addInjectionNode } from "./content/addInjectionNode";
import { addExplicitExpressionNode } from "./content/expressions/addExplicitExpressionNode";
import { addImplicitExpressionNode } from "./content/expressions/addImplicitExpressionNode";
import { addPartialNode } from "./partial/addPartialNode";
import { addForNode } from "./cascade/addForNode";
import { finishChildren } from "./finishChildren";
import { addIfNode } from "./cascade/addIfNode";

export function addFlowNode(state: BodyState): BodyState {
  switch (currentToken(state)) {
    case At:
      return addImplicitExpressionNode(state);
    case AtParenthesis:
      return addExplicitExpressionNode(state);
    case AtBrace:
      return addInjectionNode(state);
    case AtBracket:
      return addPartialNode(state);
    case For:
    case Foreach:
      return addForNode(state);
    case If:
      return addIfNode(state);
    case LineFeed1:
    case LineFeed2:
    case LineFeed3:
      return addEolNode(state);
    case Quote:
      return addQuoteNode(state);
    case Apostrophe:
      return addApostropheNode(state);
    case "}":
      return finishChildren(state);
    default: return {
      ...state,
      currentIndex: state.currentIndex + 1
    };
  }
}
