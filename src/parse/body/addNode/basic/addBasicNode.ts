import { BodyState } from "../../bodyState.type";
import { NodeType, BasicNode } from "../../../nodes/nodes.type";
import { currentPosition } from "../../tokensState/currentToken";
import { addNode } from "../addNode";

function addBasicNode(state: BodyState, type: NodeType): BodyState {
  const node = new BasicNode(type, currentPosition(state));
  return addNode(state, state.currentIndex + 1, node);
}

export function addEolNode(state: BodyState): BodyState {
  return addBasicNode(state, NodeType.Eol);
}

export function addQuoteNode(state: BodyState): BodyState {
  return addBasicNode(state, NodeType.Quote);
}

export function addApostropheNode(state: BodyState): BodyState {
  return addBasicNode(state, NodeType.Apostrophe);
}

