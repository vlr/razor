import { last } from "@vlr/array-tools";
import { isEol } from "../../common/isEol";
import { findPrevNonSpaceTokenIndex } from "../../common/isSpace";
import { ContentNode, NodeType } from "../../nodes/nodes.type";
import { BodyState } from "../bodyState.type";
import { trimNode } from "./content/tryReplaceLiteralNode";

export function getIndent(state: BodyState): string {
  return isIndented(state)
    ? lastContentNode(state).content
    : "";
}

export function removeIndent(state: BodyState): BodyState {
  return isIndented(state) ? trimNode(state) : state;
}

export function isIndented(state: BodyState): boolean {
  return isLastNodeLiteral(state) && isFirstCharacterOnTheLine(state)
    && lastContentNode(state).content.trim().length === 0;
}

export function isFirstCharacterOnTheLine(state: BodyState): boolean {
  const prev = findPrevNonSpaceTokenIndex(state.tokens, state.currentIndex - 1);
  return prev < 0 || isEol(state.tokens[prev]);
}

function isLastNodeLiteral(state: BodyState): boolean {
  const lastNode = lastContentNode(state);
  return lastNode != null && lastNode.type === NodeType.Literal;
}

function lastContentNode(state: BodyState): ContentNode {
  return <ContentNode>last(state.nodes);
}
