import { last } from "@vlr/array-tools";
import { Apostrophe, AtBrace, AtParenthesis, Quote } from "../../../tokens";
import { isPlainText } from "./isPlainText";

export const closingPairs = new Map<string, string>([
  ["{", "}"],
  ["(", ")"],
  ["[", "]"],
  ["<", ">"],
  [AtParenthesis, ")"],
  [AtBrace, "}"],
  [Quote, Quote],
  [Apostrophe, Apostrophe]
]);


export function getClosures(brackets: string[]): Map<string, number> {
  const result = new Map<string, number>();
  let current = isPlainText(last(brackets)) ? brackets.length - 1 : 0;
  while (current < brackets.length) {
    result.set(closingPairs.get(brackets[current]), current);
    current++;
  }

  return result;
}
