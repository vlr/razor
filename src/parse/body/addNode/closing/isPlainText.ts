import { contains } from "@vlr/array-tools";
import { Apostrophe, Quote } from "../../../tokens";

export const plainTextBrackets = [Quote, Apostrophe];

export function isPlainText(lastBracket: string): boolean {
  return contains(plainTextBrackets, lastBracket);
}
