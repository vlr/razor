import { TokensState } from "../../tokensState/tokensState.type";

export interface ClosingState extends TokensState {
  bracketsStack: string[];
}
