import { contains, except } from "@vlr/array-tools";
import { toMap } from "@vlr/map-tools";
import { contentTokens, restOfTokens } from "../../../../tokens";
import { BodyState } from "../../../bodyState.type";
import { findClosingTokenIndex } from "../../closing/findClosingTokenIndex";

const openingTokens = ["<", "(", "["];
const closingTokens = except([...contentTokens, ...restOfTokens], openingTokens);
const closingMap = toMap(closingTokens, item => item);

export function findImplicitExpressionEnd(state: BodyState): number {
  let current = state.currentIndex + 1;
  while (current < state.tokens.length) {
    const token = state.tokens[current];
    if (closingMap.has(token.token)) {
      break;
    }

    if (contains(openingTokens, token.token)) {
      current = findClosingTokenIndex(state.tokens, current);
    }

    current++;
  }

  return current;
}
