import { extractContent } from "../../../../common/extractContent";
import { ContentNode, NodeType } from "../../../../nodes/nodes.type";
import { BodyState } from "../../../bodyState.type";
import { currentPosition } from "../../../tokensState/currentToken";
import { addNode } from "../../addNode";
import { treatTokenAsLiteral } from "../addLiteral";
import { findImplicitExpressionEnd } from "./findImplicitExpressionEnd";

export function addImplicitExpressionNode(state: BodyState): BodyState {
  const end = findImplicitExpressionEnd(state);
  if (end - state.currentIndex < 2) {
    return treatTokenAsLiteral(state);
  }

  const content = extractContent(state.tokens, state.currentIndex + 1, end);
  const node = new ContentNode(NodeType.Expression, content, currentPosition(state));
  return addNode(state, end, node);
}
