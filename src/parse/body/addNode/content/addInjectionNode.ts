import { extractContent } from "../../../common/extractContent";
import { ContentNode, NodeType } from "../../../nodes/nodes.type";
import { BodyState } from "../../bodyState.type";
import { currentPosition } from "../../tokensState/currentToken";
import { addNode } from "../addNode";
import { findClosingTokenIndex } from "../closing/findClosingTokenIndex";
import { removeIndent } from "../getIndent";
import { getIndexAfterSpecialNode } from "../partial/getIndexAfterSpecialNode";

export function addInjectionNode(state: BodyState): BodyState {
  let end = findClosingTokenIndex(state.tokens, state.currentIndex);
  const content = extractContent(state.tokens, state.currentIndex + 1, end);

  state = removeIndent(state);
  const nextIndex = getIndexAfterSpecialNode(state, end + 1);

  const node = new ContentNode(NodeType.Injection, content, currentPosition(state));
  return addNode(state, nextIndex, node);
}
