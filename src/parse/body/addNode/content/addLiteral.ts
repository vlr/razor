
import { findIndex } from "../../../common/findIndex";
import { ContentNode, NodeType } from "../../../nodes/nodes.type";
import { Apostrophe, At, AtApostrophe, AtAt, AtQuote, Quote, EscapeBrace } from "../../../tokens";
import { Token } from "../../../types";
import { BodyState } from "../../bodyState.type";
import { isFlowToken } from "../../isFlowToken";
import { currentPosition, currentToken } from "../../tokensState/currentToken";
import { tryReplaceLiteralNode } from "./tryReplaceLiteralNode";

export function addLiteral(state: BodyState, isFlowToken: isFlowToken): BodyState {
  const index = findIndex(state.tokens, state.currentIndex, isFlowToken);
  if (index === state.currentIndex) { return state; }
  const literal = state.tokens
    .slice(state.currentIndex, index)
    .map(getTokenValue)
    .join("");
  return addOrUpdateLiteralNode(state, literal, index);
}

export function treatTokenAsLiteral(state: BodyState): BodyState {
  return addOrUpdateLiteralNode(state, currentToken(state), state.currentIndex + 1);
}

function addOrUpdateLiteralNode(state: BodyState, literal: string, end: number): BodyState {
  return tryReplaceLiteralNode(state, end, existing => {
    return new ContentNode(NodeType.Literal, existing + literal, currentPosition(state));
  });
}

const tokenMap = new Map([
  [AtAt, At],
  [AtApostrophe, Apostrophe],
  [AtQuote, Quote],
  [EscapeBrace, "}"]
]);

function getTokenValue(token: Token): string {
  return tokenMap.has(token.token)
    ? tokenMap.get(token.token)
    : token.token;
}
