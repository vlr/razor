import { Token } from "../types";

export function extractContent(tokens: Token[], start: number, end: number): string {
  return tokens
    .slice(start, end)
    .map(token => token.token)
    .join("");
}
