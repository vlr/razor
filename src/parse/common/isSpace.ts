import { Token } from "../types";
import { findIndex, findPrevIndex } from "./findIndex";

export function isSpace(token: Token): boolean {
  return token.token === " "; // might want to include tabs
}

export function findNonSpaceTokenIndex(tokens: Token[], index: number): number {
  return findIndex(tokens, index, t => !isSpace(t));
}

export function findPrevNonSpaceTokenIndex(tokens: Token[], index: number): number {
  return findPrevIndex(tokens, index, t => !isSpace(t));
}
