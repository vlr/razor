import { Token } from "../types";

export function findIndex<T>(arr: T[], index: number, condition: (item: T) => boolean): number {
  while (index < arr.length && !condition(arr[index])) { index++; }
  return index;
}

export function findPrevIndex<T>(arr: T[], index: number, condition: (item: T) => boolean): number {
  while (index >= 0 && !condition(arr[index])) { index--; }
  return index;
}

export function findTokenIndex(arr: Token[], index: number, token: string): number {
  return findIndex(arr, index, t => t.token === token);
}
