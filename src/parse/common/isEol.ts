
import { Token } from "../types";
import { LineFeed1, LineFeed2, LineFeed3 } from "../tokens";
import { contains } from "@vlr/array-tools";
import { findIndex } from "./findIndex";

export const eolTokens = [LineFeed1, LineFeed2, LineFeed3];

export function isEol(token: Token): boolean {
  return contains(eolTokens, token.token);
}

export function findEolIndex(tokens: Token[], index: number): number {
  return findIndex(tokens, index, isEol);
}
