import { contains } from "@vlr/array-tools";
import { ParsedConfig, Token } from "../types";
import { ConfigLine } from "./configLine.type";
import { Import, Using, Define, configTokens } from "../tokens";

export function isConfigToken(token: Token): boolean {
  return contains(configTokens, token.token);
}

export function updateConfig(config: ParsedConfig, line: ConfigLine): ParsedConfig {
  switch (line.startingToken.token) {
    case Import:
    case Using:
      return addImport(config, line);
    case Define:
      return updateDefinition(config, line);
    default:
      throw new Error("should never happen");
  }
}

function addImport(config: ParsedConfig, line: ConfigLine): ParsedConfig {
  return {
    ...config,
    imports: [...config.imports, { position: line.startingToken.position, content: line.config }]
  };
}

const defineRegex = /\s*([^(\s]+)\(([^\)]*)\)/;

function updateDefinition(config: ParsedConfig, line: ConfigLine): ParsedConfig {
  const match = line.config.match(defineRegex);
  if (match == null) { return config; }
  return {
    ...config,
    definition: {
      position: line.startingToken.position,
      name: match[1],
      parameters: match[2]
    }
  };
}
