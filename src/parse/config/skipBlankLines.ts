import { isEol } from "../common/isEol";
import { findNonSpaceTokenIndex } from "../common/isSpace";
import { Token } from "../types";

export function skipBlankLines(tokens: Token[], index: number): number {
  let nonSpace = findNonSpaceTokenIndex(tokens, index);
  while (index < tokens.length && isEol(tokens[nonSpace])) {
    index = nonSpace + 1;
    nonSpace = findNonSpaceTokenIndex(tokens, index);
  }

  return index;
}
