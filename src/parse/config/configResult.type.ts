import { ParsedConfig } from "../types/parsedConfig.type";

export interface ConfigResult {
  contentIndex: number;
  config: ParsedConfig;
}
