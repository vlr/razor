import { Token } from "../types";

export interface ConfigLine {
  startingToken: Token;
  config: string;
  lineLength: number;
}
