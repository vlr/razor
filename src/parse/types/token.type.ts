import { Token } from "@vlr/tokenize";
export { Token } from "@vlr/tokenize";

export function token(token: string, position: number): Token {
  return { token, position };
}
