export interface ParsedConfig {
  imports: ParsedImport[];
  definition: ParsedDefinition;
}

export interface ParsedImport {
  position: number;
  content: string;
}

export function parsedImport(position: number, content: string): ParsedImport {
  return { position, content };
}

export interface ParsedDefinition {
  position: number;
  name: string;
  parameters: string;
}

