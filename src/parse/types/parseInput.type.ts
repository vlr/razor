import { Token } from "./token.type";

export interface ParseInput {
  tokens: Token[];
  position: number;
}
