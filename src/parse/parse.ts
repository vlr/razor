import { tokenize } from "@vlr/tokenize";
import { allTokens } from "./tokens/tokens";
import { Parsed } from "./types";
import { parseConfig } from "./config/parseConfigs";
import { parseBody } from "./body/parseBody";

export function parse(template: string): Parsed {
  const tokens = tokenize(template, allTokens);
  const config = parseConfig(tokens);
  const content = parseBody(tokens, config.contentIndex);
  if (content.endPosition < tokens.length) {
    throw new Error("Error while parsing nodes, file end is not reached");
  }

  return {
    config: config.config,
    content: content.nodes
  };
}
