import { generationMark } from "./export/generationMark";

export function libMark(linefeed: string): string {
  return generationMark(linefeed, "@vlr/razor");
}
