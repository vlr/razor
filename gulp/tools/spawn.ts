import * as childProcess from "child_process";
import * as crossSpawn from "cross-spawn";
import * as shellJs from "shelljs";

const empty = [];
const inherit = { stdio: "inherit" };

export function spawn(name: string, cmd: string, parameters: string[] = empty, options: object = {}): Promise<void> {
  options = { ...inherit, ...options };
  return new Promise((resolve, reject) => {
    const spawned = spawnIt(cmd, parameters, options);
    spawned.setMaxListeners(0);
    spawned.on("close", exitCode => {
      if (exitCode === 0) {
        resolve();
      } else {
        reject(new Error("Error in task: " + name));
      }
    });
  });

}

const needsPath = ["node", "webdriver-manager"];

function spawnIt(cmd: string, parameters: string[], options: object): childProcess.ChildProcess {
  switch (process.platform) {
    case "win32":
      return crossSpawn(cmd, parameters, options);
    case "darwin":
      if (needsPath.includes(cmd)) {
        cmd = shellJs.which(cmd).toString();
      }

      return childProcess.spawn(cmd, parameters, options);

    default:
      return childProcess.spawn(cmd, parameters, options);
  }
}
